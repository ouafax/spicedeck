import streamDeck, { action, KeyDownEvent, SingletonAction, LogLevel } from "@elgato/streamdeck";
import { PinSettings } from "../settings/PinSettings";
import WebSocket from 'ws';

const logger = streamDeck.logger.createScope("Pin");

@action({ UUID: "cab.kennel.spicedeck.pin" })
export class Pin extends SingletonAction<PinSettings> {
	async onKeyDown(ev: KeyDownEvent<PinSettings>) {
		let settings = await ev.action.getSettings();
		let message = {id: 3,module: "keypads",function: "write",params: [Number(settings.player_side ?? 0), settings.pin]}
		
		let gSettings = await streamDeck.settings.getGlobalSettings()
		const ws = new WebSocket(`ws://${gSettings.spice_ip}:${gSettings.spice_port}`)
		
		ws.on('open', function open() {
			let encoded = new Uint8Array(JSON.stringify(message).split('').map(c => c.charCodeAt(0)));
			ws.send(encoded)
		})
	}
}