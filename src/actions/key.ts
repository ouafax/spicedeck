import streamDeck, { action, KeyDownEvent, SingletonAction, LogLevel } from "@elgato/streamdeck";
import { KeySettings } from "../settings/KeySettings";
import WebSocket from 'ws';

const logger = streamDeck.logger.createScope("Key");

@action({ UUID: "cab.kennel.spicedeck.key" })
export class Key extends SingletonAction<KeySettings> {
	async onKeyDown(ev: KeyDownEvent<KeySettings>) {
		logger.debug("Keypad key pressed");
		let settings = await ev.action.getSettings();
		ev.action.setTitle(`${settings.key}`);
		let message = {id: 3,module: "keypads",function: "write",params: [Number(settings.player_side ?? 0 ), settings.key ?? "0"]}
		
		let gSettings = await streamDeck.settings.getGlobalSettings()
		const ws = new WebSocket(`ws://${gSettings.spice_ip}:${gSettings.spice_port}`)

		logger.debug(JSON.stringify(message))
		ws.on('open', function open() {
			let encoded = new Uint8Array(JSON.stringify(message).split('').map(c => c.charCodeAt(0)));
			ws.send(encoded)
		})
	}
}