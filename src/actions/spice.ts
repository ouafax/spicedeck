import streamDeck, { action, KeyDownEvent, SingletonAction, LogLevel } from "@elgato/streamdeck";
import { SpiceSettings } from "../settings/SpiceSettings";

const logger = streamDeck.logger.createScope("Spice");

@action({ UUID: "cab.kennel.spicedeck.spice" })
export class Spice extends SingletonAction<SpiceSettings> {
	async onKeyDown(ev: KeyDownEvent<SpiceSettings>) {
		let settings = await ev.action.getSettings();
		
		await streamDeck.settings.setGlobalSettings({spice_ip: settings.ip, spice_port: settings.port})
	}
}