import streamDeck, { action, KeyDownEvent, SingletonAction, LogLevel } from "@elgato/streamdeck";
import WebSocket from 'ws';

import { CoinSettings } from "../settings/CoinSettings";

const logger = streamDeck.logger.createScope("coin");

@action({ UUID: "cab.kennel.spicedeck.coin" })
export class Coin extends SingletonAction<CoinSettings> {
	async onKeyDown(ev: KeyDownEvent<CoinSettings>) {
		let settings = await ev.action.getSettings();
		let message = {id:1,module:"coin",function:"insert",params:[Number(settings.coin_number ?? 1)]}
		
		let gSettings = await streamDeck.settings.getGlobalSettings()
		const ws = new WebSocket(`ws://${gSettings.spice_ip}:${gSettings.spice_port}`)
		
		ws.on('open', function open() {
			let encoded = new Uint8Array(JSON.stringify(message).split('').map(c => c.charCodeAt(0)));
			ws.send(encoded)
		})
		ev.action.setTitle(`+${settings.coin_number}`);
	}
}