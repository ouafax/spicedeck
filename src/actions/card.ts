import streamDeck, { action, KeyDownEvent, SingletonAction, LogLevel } from "@elgato/streamdeck";
import { PlayerSettings } from "../settings/PlayerSettings";
import WebSocket from 'ws';

const logger = streamDeck.logger.createScope("card");

@action({ UUID: "cab.kennel.spicedeck.card" })
export class Card extends SingletonAction<PlayerSettings> {
	async onKeyDown(ev: KeyDownEvent<PlayerSettings>) {
		let settings = await ev.action.getSettings();
		let message = {id: 2,module: "card",function: "insert",params: [Number(settings.player_side ?? 0), settings.card_id]}
		
		let gSettings = await streamDeck.settings.getGlobalSettings()
		const ws = new WebSocket(`ws://${gSettings.spice_ip}:${gSettings.spice_port}`)
		
		ws.on('open', function open() {
			let encoded = new Uint8Array(JSON.stringify(message).split('').map(c => c.charCodeAt(0)));
			ws.send(encoded)
		})
	}
}