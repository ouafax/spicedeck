import streamDeck, { LogLevel } from "@elgato/streamdeck";

import { Coin } from "./actions/coin";
import { Card } from "./actions/card";
import { Key } from "./actions/key";
import { Pin } from "./actions/pin";
import { Spice } from "./actions/spice";

// We can enable "trace" logging so that all messages between the Stream Deck, and the plugin are recorded. When storing sensitive information
streamDeck.logger.setLevel(LogLevel.TRACE);

// Register the increment action.
streamDeck.actions.registerAction(new Coin());
streamDeck.actions.registerAction(new Card());
streamDeck.actions.registerAction(new Key());
streamDeck.actions.registerAction(new Pin());
streamDeck.actions.registerAction(new Spice());

// Finally, connect to the Stream Deck.
streamDeck.connect();
