export type KeySettings = {
    player_side: number;
	key: string;
};