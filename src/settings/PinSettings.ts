export type PinSettings = {
    player_side: number;
	pin: string;
};