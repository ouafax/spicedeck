![cab.kennel.spicedeck.sdPlugin/imgs/plugin/plugin_icon.png](cab.kennel.spicedeck.sdPlugin/imgs/plugin/plugin_icon.png)

## SpiceDeck
### A SpiceTools utility inside your StreamDeck  

### Released version: 1.0

**Available features:**
* Insert Coin (x1, x10, x100)
* Insert Card (P1 or P2 / E004 or newer format)
* Keypad (P1 or P2 / 00 to 9)
* Auto Pin input

---

### How to setup:

**Be sure your streamdeck is at least on update: _6.5_**

1. Download the .streamDeckPlugin file
2. Execute it to make it install inside your streamdeck
3. Add a "Spice Setup" key. This key will allow you to set your spice's IP and PORT to make the extension able to your game.  


This plugin is working using Spice' websocket, the port is Spice Port +1   Exemple: If **1337** is your spice api, then you need to set your port to **1338**

After that, just add any key action to your streamdeck and config them, it's really easy to use.

---

### I found a bug!

If you find any bug:
* Use the issue system 
* Join the discord server: https://discord.gg/67WqqPSnnB